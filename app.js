class Employee {
    constructor(name, age, salary){
        this.name = name;
        this.age = age;
        this.salary = salary;
    }
    get name(){
        return this._name;
    }
    set name(value){
        if (value === undefined | !isNaN(value)) {
            alert("Деякі символи не можна використовувати у іменах !");
            return;
        }
        this._name = value;
    }
    get age(){
        return this._age;
    }
    set age(value){
        if (value < 18 | isNaN(value)) {
            alert("Деякі символи неприпустимі за віком !");
            return;
        }
        this._age = value;
    }    
    
    get salary(){
        return this._salary;
    }

    set salary(value){
        if (value < 50 | isNaN(value)) {
            alert("Щось не так. Будь ласка, спробуйте ще раз !");
            return;
        }
        this._salary = value;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang){
        super(name, age, salary);
        this.lang = lang;
    }
    get salary(){
        return this._salary;
    }
    set salary(value){
        super.salary = value;
        this._salary *= 3;
    }
    get lang(){
        return this._lang;
    }
    set lang(value){
        if (!Array.isArray(value)){
            return;
        }
        this._lang = value;
    }
}

let proger1 = new Programmer("Kitagava", 22, 500, ["japan"]);
let proger2 = new Programmer("James", 44, 30000, ["spanish", "germany", "english"]);
let proger3 = new Programmer("William", 10, 100, ["сhinese", "Swedish"]);

let proger4 = new Programmer("Michael", 55, 5000, ["turkish", "english"]);
let proger5 = new Programmer("Logan", 80, 300000, ["arabic", "portuguese"]);
let proger6 = new Programmer("David", 16, 1800, ["marathi", "french", "korean"]);

console.log(proger1);
console.log(proger2);
console.log(proger3);
console.log(proger4);
console.log(proger5);
console.log(proger6);